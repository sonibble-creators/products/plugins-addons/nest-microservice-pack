import { ArgsType, Field } from '@nestjs/graphql';
import { ConnectionArguments, ConnectionCursor } from 'graphql-relay';
import { getPagingParameters, PaginationPageParam } from './pagination';

/**
 * ## PaginationArgs
 *
 * the argument type of the data in connection
 * allow to send some information as argument like before, after, first, last
 * so the pagination can be managed perfectly
 *
 *
 */
@ArgsType()
export class PaginationArgs implements ConnectionArguments {
  @Field({ nullable: true, description: 'Paginate before opaque cursor' })
  public before?: ConnectionCursor;

  @Field({ nullable: true, description: 'Paginate after opaque cursor' })
  public after?: ConnectionCursor;

  @Field({ nullable: true, description: 'Paginate first' })
  public first?: number;

  @Field({ nullable: true, description: 'Paginate last' })
  public last?: number;

  // Get paging of the connection
  // by manipulate the before, after, first, last
  // if the all of pagination not defined, the default limit is become `8` and the offset is become `0`
  pagingParams(): PaginationPageParam {
    return getPagingParameters(this);
  }
}
