/**
 * # Authentication
 *
 * Allow to add some module, feature, decorator for teh auhentication
 * resource
 *
 */

// decorator
export * from './decorator/public.decorator';
export * from './decorator/current-user.decorator';
