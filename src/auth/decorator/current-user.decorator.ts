import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

/**
 * ## @CurrentUser()
 *
 * Allow you to identify the current user that request, run, and
 * doing call with the specify resource
 *
 * enable you to identify whos is the current user that request, run, and
 * btw, this only run in production, staging.
 *
 * Development environment will enable you to disable this feature
 * by default depend on the guard you use
 *
 */
export const CurrentUser: any = createParamDecorator(
  (data: unknown, context: ExecutionContext): any => {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req.user;
  },
);
