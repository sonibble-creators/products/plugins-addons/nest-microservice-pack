# CHANGELOG.md

## 1.0.1 (unreleased)

Features:

- Fully type checking feature

Bugfixs:

## 1.0.0 (2022-03-21)

Main Release:

- Adding some pagination cursor with relay support
- Custom MongoDD Mikro Orm string driver
- Auth Public, Current User resource decorator
- Adding type checking and safe
