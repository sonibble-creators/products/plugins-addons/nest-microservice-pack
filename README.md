<div id="top"></div>
<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="/">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/34654821/NestJS_Microservice_Pack.png" alt="Logo" width="100" height="100">
  </a>

  <br/>
  <br/>

  <h1 align="center">Nest Microservice Pack</h1>
  
  <br/>
  <br/>

  <p align="center">
    Pack Microservice for NestJS
    <br />
    <a href="/-/wikis"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="/">View Demo</a>
    ·
    <a href="/-/issues">Report Bug</a>
    ·
    <a href="/-/issues">Request Feature</a>
  </p>
</div>

<br />
<br />

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#about-the-project">About The Project</a></li>
    <li><a href="#wiki">Wiki</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<br />

<!-- ABOUT THE PROJECT -->
<!-- all about the project, specify the background -->

## About The Project

The NestJs Microservice pack library for creating and simple, and multi-purpose. Bring some utilities, authentication, tags, interceptor, driver, custom service, guard and more

<br/>
<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- Wiki -->
<!-- enable the user to see the wiki of this project -->

## Wiki

We build this project with some record of our documentation, If you interest to see the all about this project please check the wiki.

_For more detail, please refer to the [Wiki](/-/wikis)_

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- ROADMAP -->
<!-- Initial info roadmap of this project -->

## Roadmap

- [x] Adding some pagination cursor with relay support
- [x] Custom MongoDD Mikro Orm string driver
- [x] Auth Public, Current User resource decorator
- [x] Adding type checking and safe

See the [open issues](/-/issues) for a full list of proposed features (and known issues).

Refere to changelog to see the detail [CHANGELOG](CHANGELOG.md)

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".

Please check the contributing procedure [here](CONTRIBUTING.md), Don't forget to give the project a star! Thanks again!

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- LICENSE -->

## License

Distributed under the MIT License. See [LICENSE](LICENSE.md) for more information.

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- CONTACT -->

## Contact

Nyoman Sunima - [@nyomansunima](https://instagram.com/nyomansunima) - nyomansunima@gmail.com

Sonibble - [@sonibble](https://instagram.com/sonibble) - [creative.sonibble@gmail.com](mailto:creative.sonibble@gmail.com) - [@sonibble](https://twitter.com/sonibble)

Project Link: [DevOps Configuration](/meteo)

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>
